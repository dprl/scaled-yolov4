FROM nvcr.io/nvidia/pytorch:20.11-py3

WORKDIR /

# NOTE: Right now this is necessary because the repo is private. If you have keys on your local machine, this will use them without copying them into the container.
# 		Requires using Docker BuildKit: https://docs.docker.com/develop/develop-images/build_enhancements/#using-ssh-to-access-private-data-in-builds
# RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
# ARG CACHE_BUST=1
# RUN --mount=type=ssh git clone git@gitlab.com:dprl/scaled-yolov4.git /yolo


# INSTALL AND COMPILE MISH-CUDA
RUN git clone https://github.com/JunnYu/mish-cuda /mish-cuda
WORKDIR /mish-cuda
RUN python setup.py build install
RUN apt-get update
RUN apt-get install poppler-utils -y

# SWITCH TO YOLO DIR
WORKDIR /
RUN mkdir yolo
WORKDIR /yolo

ENV YOLO_W_MATH=best_math-lg-256dpi-896.pt
ENV YOLO_W_CHEM=best_clef-chem-yolov4-p5-256dpi-896.pt

ENV YOLO_DATA_MATH=tfd-icdar-yolo-data
ENV YOLO_DATA_CHEM=yolo_clef_data

RUN mkdir weights

# Download weights.
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_MATH} -O ./weights/${YOLO_W_MATH}
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_CHEM} -O ./weights/${YOLO_W_CHEM}

# These are for development convenience.
# COPY ./weights/best_clef-chem-yolov4-p5-256dpi-896.pt ./weights/${YOLO_W_CHEM}
# COPY ./weights/best_math-lg-256dpi-896.pt ./weights/${YOLO_W_MATH}

# Download Datasets.
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_DATA_MATH}.zip && unzip ${YOLO_DATA_MATH}.zip -d ./data
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_DATA_CHEM}.zip && unzip ${YOLO_DATA_CHEM}.zip -d ./data


#CMD python test.py --weights ./weights/${YOLO_W_MATH} --data ./data/tfd-icdar.yaml --img-size 896 --conf-thres 0.2 --nms-iou-thres 0.01 --augment

WORKDIR /

RUN mkdir output
RUN mkdir input

COPY . /yolo/
# RUN git checkout pipeline-integration

WORKDIR /yolo

# TODO Make this accept actual arguments.
ENTRYPOINT ["python", "docker-meta-runner.py", "--script"]
