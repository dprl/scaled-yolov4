import math
import tempfile
from io import StringIO
from pathlib import Path
from typing import List
import cv2
import numpy
from numpy import asarray
from pdf_annotate import PdfAnnotator, Location, Appearance
from starlette.responses import FileResponse
from torch import Tensor
from torchvision import transforms
import torch
from torchvision.transforms.functional import resize, to_tensor
from PIL.Image import Image
from PIL import Image as I
from fastapi import FastAPI, File
import uvicorn
import os
from pdf2image import convert_from_bytes
from models.experimental import attempt_load
from utils.datasets import letterbox, LoadImages
from utils.general import non_max_suppression, scale_coords, xyxy2xywh

app = FastAPI()

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

model = attempt_load(os.path.join("weights", "best_clef-chem-yolov4-p5-256dpi-896.pt"), map_location=device)
model.eval()
conf_thres=0.2
nms_iou_thres=0.01  # for NMS
iou_thres = 0.1
img_size = 896
merge = False,
half = device.type != 'cpu'
if half:
    model.half()


@app.post("/yolo")
async def extract(dpi: int, pdf: bytes = File(..., media_type="application/pdf")):
    images: List[Image] = convert_from_bytes(pdf_file=pdf, dpi=dpi)

    size_dict = {}
    with tempfile.TemporaryDirectory() as td:
        for i, image in enumerate(images):
            image.save(os.path.join(td, f"{i+1}.png"), format="png")
            size_dict[i] = (image.height, image.width)

        dataset = LoadImages(td, img_size=img_size)
        csv_buffer = StringIO()
        img = torch.zeros((1, 3, img_size, img_size), device=device)  # init img
        _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
        for path, img, im0s, vid_cap in dataset:
            # Original img used for:
            #   - Getting original image height and width to denormalize detections
            #   - Drawing detections over copy of original image to verify rescaling works.
            original_img = cv2.imread(path)
            original_img_0 = asarray(original_img)
            original_img_h, original_img_w, _ = original_img.shape  # discard channel info
            img = torch.from_numpy(img).to(device)
            img = img.half() if half else img.float()  # uint8 to fp16/32
            img /= 255.0  # 0 - 255 to 0.0 - 1.0
            if img.ndimension() == 3:
                img = img.unsqueeze(0)

            im0 = im0s
            # Inference
            pred = model(img, augment=False)[0]
            pred = non_max_suppression(pred, conf_thres=conf_thres, iou_thres=iou_thres,
                                       agnostic=False)
            for i, det in enumerate(pred):
                gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]
                if det is not None and len(det):
                    # Rescale boxes from img_size to im0 size
                    val = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()
                    det[:, :4] = val
                    for *xyxy, conf, cls in det:
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh

                        # Rescale by original image dimensions to write output
                        unnormalized_xywh = [xywh[0] * original_img_w, xywh[1] * original_img_h,
                                             xywh[2] * original_img_w, xywh[3] * original_img_h]
                        half_w = unnormalized_xywh[2] / 2
                        half_h = unnormalized_xywh[3] / 2
                        top_left = (float(unnormalized_xywh[0] - half_w), float(unnormalized_xywh[1] - half_h))
                        bottom_right = (float(unnormalized_xywh[0] + half_w), float(unnormalized_xywh[1] + half_h))

                        csv_buffer.write((str(int(Path(path).stem)-1) + '.00,' + '%.2f,' * 3 + '%.2f' + '\n') % (
                            *top_left, *bottom_right))
    return {"size_dict": size_dict, "csv": csv_buffer.getvalue()}


@app.post("/visualize")
async def visualize(dpi: int, pdf: bytes = File(..., media_type="application/pdf")):

    dict = await extract(dpi, pdf)
    pdf_file_out_path = tempfile.NamedTemporaryFile(delete=False).name
    with tempfile.TemporaryDirectory() as td:
        pdf_file_path = os.path.join(td, "test.pdf")
        with open(pdf_file_path, "wb") as f:
            f.write(pdf)
        a = PdfAnnotator(pdf_file_path)
        for line in dict["csv"].split('\n'):
            print()
            parts = line.split(",")
            if len(parts) < 2:
                continue
            page = int(float(parts[0]))

            page_size = dict["size_dict"][page]
            height = float(page_size[0])
            width = page_size[1]

            # need to perform inversion due to PDF origin being bottom left as opposed to image origin of top right
            top_l_x = float(parts[1]) * 72/300
            top_l_y = (height - float(parts[4])) * 72/300
            bottom_r_x = float(parts[3]) * 72/300
            bottom_r_y = (height - float(parts[2])) * 72/300

            print(top_l_x, top_l_y, bottom_r_x, bottom_r_y)
            print(height, width)
            a.add_annotation(
                'square',
                Location(x1=top_l_x, y1=top_l_y, x2=bottom_r_x, y2=bottom_r_y, page=page),
                Appearance(stroke_color=(1, 0, 0), stroke_width=1),
            )
        a.write(pdf_file_out_path)
        return FileResponse(
            pdf_file_out_path,
            media_type='application/pdf',
            filename="visualized"
        )


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8005)
