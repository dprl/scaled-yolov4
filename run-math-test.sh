#!bin/sh
YOLO_W_MATH="best_math-lg-256dpi-896.pt"

use_sudo=false

# Check for sudo flag from user.
while getopts 's' flag; do
	case "${flag}" in
		s) use_sudo=true ;;
		*) error "Unexpected option \"${flag}\"" ;;
	esac
done

# Report message
mymsg () {
	echo "    >> $@"
	echo ""
}

# Used to execute a command in YOLO docker container. Assumes container is named the same as the install script would name it.
yolodockerexec() {
	if [ "${use_sudo}" = true ]
	then
		sudo docker exec yolo /bin/sh -c "$@"
	else
		docker exec yolo /bin/sh -c "$@"
	fi
}

sudo_str=""

if [ "${use_sudo}" = true ]
then
	sudo_str="sudo "
fi

if ${sudo_str}docker ps -a | grep yolo > /dev/null
then
	${sudo_str}docker start yolo
	yolodockerexec "python test.py --weights weights/${YOLO_W_MATH} --data data/tfd-icdar.yaml --img-size 896 --conf-thres 0.2 --nms-iou-thres 0.01 --augment"
	${sudo_str}docker stop yolo
else
	mymsg "YOLO docker container does not exist, please run the included install.sh."
fi

