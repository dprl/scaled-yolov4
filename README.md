# YOLOv4-large

This is the DPRL modified implementation of "[Scaled-YOLOv4: Scaling Cross Stage Partial Network](https://arxiv.org/abs/2011.08036)" using PyTorch framwork.


## Original Repos
* [YOLOv4-CSP](https://github.com/WongKinYiu/ScaledYOLOv4/tree/yolov4-csp)
* [YOLOv4-tiny](https://github.com/WongKinYiu/ScaledYOLOv4/tree/yolov4-tiny)
* [YOLOv4-large](https://github.com/WongKinYiu/ScaledYOLOv4/tree/yolov4-large)

## Installation

To install a conda environment run the following commands
```commandline
conda create -n scaledyolo python=3.6.10
conda activate scaledyolo
pip install -r requirements.txt

git clone https://github.com/JunnYu/mish-cuda mish-cuda
cd mish-cuda
```
This was developed and tested with docker engine 24.0.2 and nvidia container toolkit 1.13.1

To do local development within the docker container run `docker run -it --rm --runtime=nvidia --gpus all nvcr.io/nvidia/pytorch:20.11-py3 sh`

Run `docker buildx build -t yolo .` in this directory to build the container. This will take a bit, but will grab all the training and testing data you need to validate results and test detections.

To build the server container run `docker buildx build -t dprl/yoloserver:latest -f server.Dockerfile .`
After the image is built run `docker run --runtime=nvidia --gpus all --shm-size=64g -p 8005:8005 yolo-server` to deploy the server. go to `http://localhost:8005/docs` to view the Swagger API docs.

To run the various operations supported by the container, use this format:

```
docker run --runtime=nvidia --gpus all --shm-size=64g yolo <script-name> [...args]
```

## Supported Scripts and Arguments:
| **Script Name** | **Arguments** | **Description**                                    |
|-----------------|---------------|----------------------------------------------------|
| `math_test`     | None          | Run a test on the TDF-ICDAR2019 math test set.     |
| `chem_test`     | None          | Run a test on the CLEF-IP 2012 chemistry test set. |

For example `docker run --runtime=nvidia --gpus all --shm-size=64g yolo chem_test` will run the chem_test script with default arguments
## Running Detections
For detection, a helper script is provided. This script will handle all of the necessary bind mounting for input/output directories for you. To do this, use the `docker_detection.py` script.

*Note:* This script should be run from **outside** the docker container. It will call `docker run` as needed.

Run the script as so:

```
python docker_detection.py --input-dir <path_to_input> --output_dir <path_to_output> --model <math, chem> [--multi_dir]
```

### Arguments explained:
| **Argument**   | **Description**                                                                                                                                                                                                                                                                                                      |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `--input-dir`  | Path to input directory on local file system. Will be bind-mounted to docker container. If `--multi_dir` is passed, this should be a directory with subdirectories, each representing a converted PDF document and containing one image per page. If not `--multi_dir`, this should be a directory with page images. |
| `--output-dir` | Directory to save output to on local file system.                                                                                                                                                                                                                                                                    |
| `--model`      | ``math`` or ``chem``. Choose which trained weights to use for detection.                                                                                                                                                                                                                                             |
| `--multi_dir`  | Whether to look for multiple documents (directories) in the provided input directory. Useful when you want to run detection on a batch of multiple documents.                                                                                                                                                        |

## Citation

```
@InProceedings{Wang_2021_CVPR,
    author    = {Wang, Chien-Yao and Bochkovskiy, Alexey and Liao, Hong-Yuan Mark},
    title     = {{Scaled-YOLOv4}: Scaling Cross Stage Partial Network},
    booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
    month     = {June},
    year      = {2021},
    pages     = {13029-13038}
}
```

## Acknowledgements

<details><summary> <b>Expand</b> </summary>

* [https://github.com/AlexeyAB/darknet](https://github.com/AlexeyAB/darknet)
* [https://github.com/WongKinYiu/PyTorch_YOLOv4](https://github.com/WongKinYiu/PyTorch_YOLOv4)
* [https://github.com/ultralytics/yolov3](https://github.com/ultralytics/yolov3)
* [https://github.com/ultralytics/yolov5](https://github.com/ultralytics/yolov5)

</details>
