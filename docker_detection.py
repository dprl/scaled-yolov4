import os
import argparse
import subprocess


def gen_docker_bind_arg(source_dir, target_dir):
    return f"--mount type=bind,source={os.path.join(os.getcwd(), source_dir)},target={target_dir}"


def main(args):
    detection_type = "detect_math" if args.model == "math" else "detect_chem"
    if args.multi_dir:
        detection_type += "_multi"

    if args.save_imgs:
        detection_type += "_save_img"

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir, exist_ok=True)

    docker_command = f"nvidia-docker run \
            {gen_docker_bind_arg(args.input_dir, '/input')} \
            {gen_docker_bind_arg(args.output_dir, '/output')} \
            yolo {detection_type}"

    print(gen_docker_bind_arg(args.input_dir, '/input'))
    print(gen_docker_bind_arg(args.output_dir, '/output'))

    subprocess.call(docker_command, shell=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Top level script to run YOLO scripts from docker run.")

    parser.add_argument("--input_dir", type=str, required=True, help="Input directory to run inference on.")
    parser.add_argument("--output_dir", type=str, required=True, help="Output directory to save detection results to.")
    parser.add_argument("--model", type=str, required=True, choices=["math", "chem"], help="Model type to use.")
    parser.add_argument("--multi_dir", action="store_true", help="Detection on multiple documents, use when input "
                                                                 "directory contains multiple "
                                                                 "subdirectories/documents of images.")
    parser.add_argument("--save-imgs", action="store_true", help="Save detection outputs as images.")

    args = parser.parse_args()
    main(args)

