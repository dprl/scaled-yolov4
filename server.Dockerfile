FROM nvcr.io/nvidia/pytorch:20.11-py3

WORKDIR /

# NOTE: Right now this is necessary because the repo is private. If you have keys on your local machine, this will use them without copying them into the container.
# 		Requires using Docker BuildKit: https://docs.docker.com/develop/develop-images/build_enhancements/#using-ssh-to-access-private-data-in-builds
# RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
# ARG CACHE_BUST=1
# RUN --mount=type=ssh git clone git@gitlab.com:dprl/scaled-yolov4.git /yolo

# INSTALL AND COMPILE MISH-CUDA
RUN git clone https://github.com/JunnYu/mish-cuda /mish-cuda
WORKDIR /mish-cuda
RUN python setup.py build install

RUN apt update
RUN apt install poppler-utils -y
# SWITCH TO YOLO DIR
WORKDIR /

RUN mkdir yolo
WORKDIR /yolo

ENV YOLO_W_MATH=best_math-lg-256dpi-896.pt
ENV YOLO_W_CHEM=best_clef-chem-yolov4-p5-256dpi-896.pt

ENV YOLO_DATA_MATH=tfd-icdar-yolo-data
ENV YOLO_DATA_CHEM=yolo_clef_data

RUN mkdir weights

# Download weights.
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_MATH} -O ./weights/${YOLO_W_MATH}
RUN wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_CHEM} -O ./weights/${YOLO_W_CHEM}

COPY . .
# RUN git checkout pipeline-integration
RUN pip install -r requirements.txt
RUN pip uninstall typing_extensions -y
RUN pip uninstall fastapi -y
RUN pip install --no-cache fastapi

EXPOSE 8005

WORKDIR /yolo

CMD ["python", "server.py"]