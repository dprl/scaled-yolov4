import argparse
import os
import platform
import shutil
import stat
import time
from pathlib import Path

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random, asarray

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords, xyxy2xywh, plot_one_box, strip_optimizer)
from utils.torch_utils import select_device, load_classifier, time_synchronized

YOLO_IN_DIR = '/input'
YOLO_OUT_DIR = '/output'


def detect(save_img=False, multi_source_dir=None):
    out, source, weights, view_img, save_txt, imgsz, save_img = \
        opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size, opt.save_img
    webcam = source == '0' or source.startswith('rtsp') or source.startswith('http') or source.endswith('.txt')

    output_buffer = []
    
    # HACK: This is commandeering the existing source directory to pass in a source directory of our choosing.
    #       Useful for iterating over multiple directories.
    if multi_source_dir:
        source = source + multi_source_dir
    
    txt_path = str(Path(out) / os.path.basename(source))
    save_path = str(Path(out) / os.path.basename(source))

    if save_img:
        if os.path.exists(txt_path) and os.path.isdir(txt_path):
            for file in os.listdir(txt_path):
                os.remove(os.path.join(txt_path, file))
        else:
            os.mkdir(txt_path)

    # Initialize
    device = select_device(opt.device)
    # if os.path.exists(out):
    #   shutil.rmtree(out)  # delete output folder
    if not os.path.exists(out):
        os.makedirs(out)  # make new output folder
        os.chmod(out, 0o777)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
    dataset = LoadImages(source, img_size=imgsz)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
    for path, img, im0s, vid_cap in dataset:
        # Original img used for:
        #   - Getting original image height and width to denormalize detections
        #   - Drawing detections over copy of original image to verify rescaling works.
        original_img = cv2.imread(path)
        original_img_0 = asarray(original_img)
        original_img_h, original_img_w, _ = original_img.shape  # discard channel info
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, conf_thres=opt.conf_thres, iou_thres=opt.iou_thres, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()
        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)
        
        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            # NOTE: Right now this is a bit of a hack to get everything to merge into one csv per document. Need to circle back and
            #       figure out how this will be used in production. - JP Mar 22, 2022
            save_path = str(Path(out) / Path(p).parent)
            img_save_path = os.path.join(txt_path, Path(p).name) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string

                # Write results
                for *xyxy, conf, cls in det:
                    xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh

                    # Rescale by original image dimensions to write output
                    unnormalized_xywh = [xywh[0] * original_img_w, xywh[1] * original_img_h, xywh[2] * original_img_w, xywh[3] * original_img_h]
                    half_w = unnormalized_xywh[2] / 2
                    half_h = unnormalized_xywh[3] / 2
                    top_left = (float(unnormalized_xywh[0] - half_w), float(unnormalized_xywh[1] - half_h))
                    bottom_right = (float(unnormalized_xywh[0] + half_w), float(unnormalized_xywh[1] + half_h))

                    if save_txt:  # Write to file
                        output_buffer.append((str(int(Path(p).stem) - 1) + '.00,' + '%.2f,' * 3 + '%.2f' + '\n') % (*top_left, *bottom_right))
                        # with open(txt_path + '.csv', 'a') as f:
                        #    f.write((str(int(Path(p).stem) - 1) + '.00,' + '%.2f,' * 3 + '%.2f' + '\n') % (*top_left, *bottom_right))  # label format (note, trailing comma needed or python complains.)
                        #    os.chmod(txt_path + '.csv', 0o777) 

                    if save_img or view_img:  # Add bbox to image
                        label = '%s' % (names[int(cls)])
                        plot_one_box([*top_left, *bottom_right], original_img_0, label=label, color=colors[int(cls)], line_thickness=2)

            # Print time (inference + NMS)
            print('%sDone. (%.3fs)' % (s, t2 - t1))

            # Stream results
            if view_img:
                cv2.imshow(p, im0)
                if cv2.waitKey(1) == ord('q'):  # q to quit
                    raise StopIteration

            # Save results (image with detections)
            if save_img:
                if dataset.mode == 'images':
                    print(f"Writing image to {img_save_path}")
                    cv2.imwrite(img_save_path, original_img_0)
                else:
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer

                        fourcc = 'mp4v'  # output video codec
                        fps = vid_cap.get(cv2.CAP_PROP_FPS)
                        w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                        h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
                    vid_writer.write(im0)

    if save_txt or save_img:
        if os.path.exists(txt_path + ".csv"):
            os.remove(txt_path + ".csv")
        with open(txt_path + '.csv', 'a') as f:
            print(f"Writing {len(output_buffer)} detections to {txt_path}.csv")
            f.writelines(output_buffer)
        print('Results saved to %s' % Path(out))
        if platform == 'darwin' and not opt.update:  # MacOS
            os.system('open ' + save_path)

    print('Done. (%.3fs)' % (time.time() - t0))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov4-p5.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='inference/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--output', type=str, default='inference/output', help='output folder')  # output folder
    parser.add_argument('--img-size', type=int, default=896, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.2, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.01, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--save-img', action="store_true", help="save image detection results to output folder")
    parser.add_argument('--multi_dir', action="store_true", help="Running in pipeline context, assume multiple documents' worth of input.")
    opt = parser.parse_args()
    print(opt)

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['']:
                detect()
                strip_optimizer(opt.weights)
        elif opt.multi_dir:
            start = time.time()
            # If multi_dir is true, assume we need to run in multiple subdirectories (each representing a converted document)
            for item in os.scandir(YOLO_IN_DIR):
                if item.is_dir():
                    detect(multi_source_dir=item.name)
            print('Done. (%.3fs)' % (time.time() - start))
        else:
            detect()
