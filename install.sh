#!/bin/sh

YOLO_W_MATH="best_math-lg-256dpi-896.pt"
YOLO_W_CHEM="best_clef-chem-yolov4-p5-256dpi-896.pt"

YOLO_DATA_MATH="tfd-icdar-yolo-data"
YOLO_DATA_CHEM="yolo_clef_data"

use_sudo=false

# Check for sudo flag from user.
while getopts 's' flag; do
	case "${flag}" in
		s) use_sudo=true ;;
		*) error "Unexpected option \"${flag}\"" ;;
	esac
done

# Report message
mymsg () {
	echo "    >> $@"
	echo ""
}

# Used to execute a command in YOLO docker container. Assumes container is named the same as this script would name it.
yolodockerexec() {
	if [ "${use_sudo}" = true ]
	then
		sudo docker exec yolo /bin/sh -c "$@"
	else
		docker exec yolo /bin/sh -c "$@"
	fi
}

################################################################
# WEIGHTS
################################################################

if [ -d ./weights ]
then
	mymsg "Weights directory already exists."
else
	mkdir weights
fi

echo ""
echo "[ Downloading YOLO weights ]"
if [ -f ./weights/${YOLO_W_MATH} ]
then
	mymsg "YOLO math weights already downloaded."
else
	wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_MATH} -O ./weights/${YOLO_W_MATH}
fi

if [ -f ./weights/${YOLO_W_CHEM} ]
then
	mymsg "YOLO chem weights already downloaded."
else
	wget https://www.cs.rit.edu/~dprl/data/${YOLO_W_CHEM} -O ./weights/${YOLO_W_CHEM}
fi


################################################################
# DATASETS 
################################################################

echo ""
echo "[ Downloading Test Data ]"
if [ -d ./data/${YOLO_DATA_MATH} ]
then
	mymsg "TFD-ICDAR data already downloaded."
else
	wget https://www.cs.rit.edu/~dprl/data/${YOLO_DATA_MATH}.zip && unzip ${YOLO_DATA_MATH}.zip -d ./data
fi

if [ -d ./data/${YOLO_DATA_CHEM} ]
then
	mymsg "CLEF-2012 IP data already downloaded."
else
	wget https://www.cs.rit.edu/~dprl/data/${YOLO_DATA_CHEM}.zip && unzip ${YOLO_DATA_CHEM}.zip -d ./data
fi

# Generate YOLO Docker Environment
echo "[ Generating YOLO Docker Environment ]"
# NOTE: Not sure if we can get away with a smaller shm-size here, have not tested.
# TODO: Figure out where test data should go.

if [ "${use_sudo}" = true ]
then
	sudo nvidia-docker run -dit --name yolo -v $PWD/:/yolo -w /yolo --shm-size=64g nvcr.io/nvidia/pytorch:20.11-py3
else
	nvidia-docker run -dit --name yolo -v $PWD/:/yolo -w /yolo --shm-size=64g nvcr.io/nvidia/pytorch:20.11-py3
fi

# Now in docker container, install deps.
yolodockerexec "git clone https://github.com/JunnYu/mish-cuda /mish-cuda"
yolodockerexec "cd /mish-cuda && python setup.py build install" # NOTE: cd into mish-cuda is necessary for mish-cuda install scripts to work correctly

# Stop container? Doing this for now so it's not just running, will need to restart in python code later?

if [ "${use_sudo}" = true ]
then
	sudo docker stop yolo
else
	docker stop yolo
fi
