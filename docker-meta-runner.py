import argparse
import subprocess

SAVE_IMG = "_save_img"

DETECT_MATH = "detect_math"
DETECT_CHEM = "detect_chem"

DETECT_MATH_MULTI = DETECT_MATH + "_multi"
DETECT_CHEM_MULTI = DETECT_CHEM + "_multi"

DETECT_MATH_SAVE_IMGS = DETECT_MATH + SAVE_IMG
DETECT_CHEM_SAVE_IMGS = DETECT_CHEM + SAVE_IMG

DETECT_MATH_MULTI_SAVE_IMGS = DETECT_MATH_MULTI + SAVE_IMG
DETECT_CHEM_MULTI_SAVE_IMGS = DETECT_CHEM_MULTI + SAVE_IMG

MATH_TEST_STR = "math_test"
CHEM_TEST_STR = "chem_test"

MATH_MODEL = "math"
CHEM_MODEL = "chem"

model_weights= {
    MATH_MODEL: "best_math-lg-256dpi-896.pt",
    CHEM_MODEL: "best_clef-chem-yolov4-p5-256dpi-896.pt"
}

# NOTE: This assumes that a bind mount is set up connecting the user's desired input folder to /input in the container, and the same for /output
script_types = {
    DETECT_MATH: f"detect.py --weights ./weights/{model_weights[MATH_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --augment --iou-thres 0.01 --conf-thres 0.2",
    DETECT_CHEM: f"detect.py --weights ./weights/{model_weights[CHEM_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --iou-thres 0.01 --conf-thres 0.2",
    DETECT_MATH_MULTI: f"detect.py --weights ./weights/{model_weights[MATH_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --multi_dir --iou-thres 0.01 --conf-thres 0.2 --augment",
    DETECT_CHEM_MULTI: f"detect.py --weights ./weights/{model_weights[CHEM_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --multi_dir --iou-thres 0.01 --conf-thres 0.2",
    DETECT_MATH_SAVE_IMGS: f"detect.py --weights ./weights/{model_weights[MATH_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --augment --iou-thres 0.01 --conf-thres 0.2 --save-img",
    DETECT_CHEM_SAVE_IMGS: f"detect.py --weights ./weights/{model_weights[CHEM_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --iou-thres 0.01 --conf-thres 0.2 --save-img",
    DETECT_MATH_MULTI_SAVE_IMGS: f"detect.py --weights ./weights/{model_weights[MATH_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --multi_dir --iou-thres 0.01 --conf-thres 0.2 --augment --save-img",
    DETECT_CHEM_MULTI_SAVE_IMGS: f"detect.py --weights ./weights/{model_weights[CHEM_MODEL]} --img-size 896 --source /input/ --output /output/ --save-txt --multi_dir --iou-thres 0.01 --conf-thres 0.2 --save-img",
    MATH_TEST_STR: f"test.py --weights weights/{model_weights[MATH_MODEL]} --data data/tfd-icdar.yaml --img-size 896 --conf-thres 0.2 --nms-iou-thres 0.01 --augment",
    CHEM_TEST_STR: f"test.py --weights weights/{model_weights[CHEM_MODEL]} --data data/clef_2012.yaml --img-size 896 --conf-thres 0.2 --nms-iou-thres 0.01"
}


def main(args):
    subprocess.call(f"python {script_types[args.script]}", shell=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Top level script to run YOLO scripts from docker run.")

    parser.add_argument("--script", type=str, help=f"Script type to run.", choices=script_types.keys())

    args = parser.parse_args()
    main(args)

